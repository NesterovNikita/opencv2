﻿#include <iostream> 
#include <vector> 
#include <cmath> 
#include <opencv2\opencv.hpp> 
#include <algorithm> 
#include <opencv2/calib3d.hpp> 
#include <opencv2/core.hpp> 
#include <opencv2/imgcodecs.hpp> 
#include <opencv2/xfeatures2d.hpp> 
#include <opencv2/xfeatures2d/nonfree.hpp> 
#include <opencv2/imgproc/imgproc.hpp> 
#include <opencv2/highgui/highgui.hpp> 
#include <opencv2/core/utility.hpp> 

using namespace cv;
using namespace cv::xfeatures2d;

size_t findMinIndex(std::vector<double>& data) {
    size_t min = 0;
    for (size_t i = 1; i != data.size(); ++i) {
        if (data[min] > data[i]) {
            min = i;
        }
    }
    return min;
}



double distance_between_words(std::vector <double>& base, std::vector<double>& input, int dimencion) {
    double distance = 0;
    for (int i = 0; i < dimencion; ++i) {
        distance += ((double)(base[i] - input[i]) * (double)(base[i] - input[i]));
    }
    return distance;
}

void Storage_visual_words(std::vector<std::vector<int>> words) {
    FileStorage fs_visual_words("data/fs_visual_Words.yaml", FileStorage::WRITE);
    fs_visual_words << "visual_Words" << words;
    fs_visual_words.release();
}

void Storage_centers(Mat centers) {
    FileStorage fs_centers("data/fs_centers.yaml", FileStorage::WRITE);
    fs_centers << "centers" << centers;
    fs_centers.release();
}
/*
void Storage_descriptors(Mat descriptor) {
    FileStorage fs_descriptor("data/fs_descriptor.yaml", FileStorage::WRITE);
    fs_descriptor << "descriptor" << descriptor;
    fs_descriptor.release();
}
*/
void Storage_points(std::vector<int> data_points) {
    FileStorage fs_points("data/fs_points.yaml", FileStorage::WRITE);
    fs_points << "points" << data_points;
    fs_points.release();
}

void Storage_labels(std::vector<int> labels) {
    FileStorage fs_labels("data/fs_labels.yaml", FileStorage::WRITE);
    fs_labels << "labels" << labels;
    fs_labels.release();
}
int main() {
    Mat list;
    Ptr<Feature2D> f2d_sift = SIFT::create(0, 3, 0.18, 5, 1.6); // trying cycles 
    /*
    std::vector<int> data_points(2000, 0);
    for (size_t i = 0; i != 2000; ++i) {
        std::string filename = "kinopoisk/" + std::to_string(i) + ".jpg";
        Mat image = imread(filename,CV_LOAD_IMAGE_UNCHANGED);
        if (image.empty()) {
            continue;
        }
        std::vector<KeyPoint> points_one;
        Mat descriptor;
        f2d_sift->detectAndCompute(image, Mat(), points_one, descriptor);
        // Mat sift_keys; 
        data_points[i] = points_one.size();
        list.push_back(descriptor);
    }
    int K = list.rows / 80; // кол-во кластеров 
    Mat centers; // матрица, с K строками. В каждой строке 128 координат(потому что сифт). 
    std::vector<int> labels(list.rows); //вектор на кластерах все особые точки во всех картинках. 
    kmeans(list, K, labels, cvTermCriteria(CV_TERMCRIT_NUMBER + CV_TERMCRIT_EPS, 10, 1.0), 1, KMEANS_PP_CENTERS, centers);
    */
    /*
    FileStorage fs_centers("data/fs_centers.yaml",FileStorage::WRITE);
    fs_centers << "cluster_centers" << centers;
    fs_centers.release();
    */
    /*
    int count = 0;
    std::vector<std::vector<int>> words(data_points.size(), std::vector<int>(K, 0));
    for (int i = 0; i < data_points.size(); ++i) {
        for (int j = 0; j < data_points[i]; ++j) {
            words[i][labels[count]]++;
            count++;
        }
    }

    */
    /*
    Storage_centers(centers);
    //Storage_descriptors(list);
    Storage_visual_words(words);   // Делаем 1 раз. создаём базу. 
    Storage_points(data_points);
    Storage_labels(labels);
    */
    
    std::vector<int> data_points;
    Mat centers;
    std::vector<std::vector<int>> words;
    std::vector<int> labels;

    FileStorage fs_centers("data/" + "fs_centers.yaml", FileStorage::READ);
    fs_centers["centers"] >> centers;
    fs_centers.release();

    FileStorage fs_points("data/" + "fs_points.yaml", FileStorage::READ);
    fs_points["points"] >> data_points;
    fs_points.release();

    FileStorage fs_visual_words("data/" + "fs_visual_words.yaml", FileStorage::READ);
    fs_visual_words["visual_words"] >> words;
    fs_visual_words.release();

    FileStorage fs_labels("data/" + "fs_labels.yaml", FileStorage::READ);
    fs_labels["labels"] >> labels;
    fs_labels.release();

    std::vector<std::vector<double>> TF(data_points.size(), std::vector<double>(K, 0));

    for (int i = 0; i < TF.size(); ++i) { // TF[i]- тф представление итой картинки. 
        for (int j = 0; j < TF[i].size(); ++j) {
            TF[i][j] = (double)words[i][j] / (double)data_points[i];
        }
    }
    
    std::vector<double> cluster_shmaster(K, 0);
    for (int i = 0; i < words.size(); ++i) {
        for (int j = 0; j < words[i].size(); ++j) {
            if (words[i][j] != 0)
                cluster_shmaster[j]++;
        }
    }

    std::vector<std::vector<double>> IDF(data_points.size(), std::vector<double>(K, 0));
    for (int i = 0; i < IDF.size(); ++i) {
        for (int j = 0; j < IDF[i].size(); ++j) { // IDF[i]- idf представление итой картинки. 
            IDF[i][j] = (double)log((double)data_points.size() / cluster_shmaster[j]);
        }
    }

    std::vector<std::vector<double>> TF_IDF(data_points.size(), std::vector<double>(K));
    for (int i = 0; i < IDF.size(); ++i) {
        for (int j = 0; j < IDF[i].size(); ++j) {
            TF_IDF[i][j] = TF[i][j] * IDF[i][j];
        }
    }
    /*
    std::vector<std::vector<int>> base(K);
    for (int i = 0; i < K; ++i) {
        for (int j = 0; j < data_points.size(); j++) {
            if (TF[j][i] != 0)
                base[i].push_back(j);
        }
    }
    */
    Mat test = imread("kinopoisk/4.jpg", CV_LOAD_IMAGE_UNCHANGED);
    if (test.empty())
        std::cout << "error";
    std::vector<KeyPoint> test_points_one;
    Mat test_descriptor;
    f2d_sift->detectAndCompute(test, Mat(), test_points_one, test_descriptor);
    std::vector<int> test_labels(test_descriptor.rows);
    std::vector<std::vector<double>> distances(test_descriptor.rows, std::vector<double>(centers.rows, 0));

    double distance = 0;
    for (size_t j = 0; j != test_descriptor.rows; ++j) {
        float *currentDot = test_descriptor.ptr<float>(j);
        for (size_t i = 0; i != centers.rows; ++i) {
            float *currentRow = centers.ptr<float>(i);
            distance = normL2Sqr(currentDot, currentRow, test_descriptor.cols);
            distances[j][i] = distance; // расстояние между j-ой точкой и i-тым кластером. 
        }
    }

    for (size_t i = 0; i != test_labels.size(); ++i) {
        test_labels[i] = findMinIndex(distances[i]);
    }

    std::vector<double> test_visualWord(K, 0);
    for (int i = 0; i < test_labels.size(); ++i) {
        ++test_visualWord[test_labels[i]];
    }

    std::vector<double> test_TF_IDF(K, 0);
    for (int i = 0; i < K; ++i) {
        test_TF_IDF[i] = ((double)test_visualWord[i] * IDF[0][i]) / ((double)test_labels.size());
    }

    for (int i = 0; i != test_visualWord.size(); ++i) {
        if (test_visualWord[i] != 0) {
            cluster_shmaster[i] += 1;
        }
        test_visualWord[i] = (test_visualWord[i] / test_descriptor.rows) * log((data_points.size()+1) / cluster_shmaster[i]);
    }


    int index = -1;
    double line = INT_MAX;
    for (int i = 0; i < data_points.size(); ++i) {
        double between = distance_between_words(TF_IDF[i], test_TF_IDF, K);
        if (line > between) {
            index = i;
            line = between;
        }
    }

    std::cout << index << std::endl;
    waitKey();
}
