﻿/*#include <opencv2/opencv.hpp>
using namespace cv;
using std::cout;



int main()
{
    
    Mat image;
    Mat reversed;
    image = imread("girl.jpg", CV_LOAD_IMAGE_UNCHANGED);

    if (!image.data)
    {
        cout << "Could not open or find the image" << std::endl;
        return 0;
    }

    namedWindow("Display window", CV_WINDOW_AUTOSIZE);
    imshow("Display window", image);

    imwrite("reversedgirl.jpg", reversed);
    Point2f src_center(image.cols / 2.0, image.rows / 2.0);
    Mat rotation = getRotationMatrix2D(src_center, 30, 2.0 / 3);
    Mat rotated;
    warpAffine(image, rotated, rotation, image.size());
    Mat gray;
    Mat kp1, kp2;
    cvtColor(image, gray, CV_BGR2GRAY);
    
    int threshold1 = 5;
    std::vector<KeyPoint> keypoints1;
    FAST(gray, keypoints1, threshold1, true);
    drawKeypoints(image, keypoints1, kp1);
    

    Mat src, src_gray;
    Mat grad;
    int scale = 1;
    int delta = 0;
    int ddepth = CV_16S;

    int c;

    /// Load an image
    src = imread("girl.jpg", CV_LOAD_IMAGE_UNCHANGED);

    
    GaussianBlur(src, src, Size(3, 3), 0, 0, BORDER_DEFAULT);

    /// Convert it to gray
    cvtColor(src, src_gray, CV_BGR2GRAY);

    /// Create window
    namedWindow("window_name", CV_WINDOW_AUTOSIZE);

    /// Generate grad_x and grad_y
    Mat grad_x, grad_y;
    Mat abs_grad_x, abs_grad_y;

    /// Gradient X
    //Scharr( src_gray, grad_x, ddepth, 1, 0, scale, delta, BORDER_DEFAULT );
    Sobel(src_gray, grad_x, ddepth, 1, 0, 3, scale, delta, BORDER_DEFAULT);
    convertScaleAbs(grad_x, abs_grad_x);

    /// Gradient Y
    //Scharr( src_gray, grad_y, ddepth, 0, 1, scale, delta, BORDER_DEFAULT );
    Sobel(src_gray, grad_y, ddepth, 0, 1, 3, scale, delta, BORDER_DEFAULT);
    convertScaleAbs(grad_y, abs_grad_y);

    /// Total Gradient (approximate)
    addWeighted(abs_grad_x, 0.5, abs_grad_y, 0.5, 0, grad);

    for (size_t i = 0; i != grad.rows; ++i) {
        for (size_t j = 0; j != grad.cols; ++j) {
            grad.at<uchar>(i, j) = sqrt(pow(abs_grad_x.at<uchar>(i, j), 2) + pow(abs_grad_y.at<uchar>(i, j), 2));
        }
    }

    imshow("window_name", grad);
    imwrite("gradgirlsobel.jpg", grad);

    
    waitKey(0);
    
    return 0;
}
*/