﻿#include <iostream>
#include <vector>
#include <cmath>
#include <opencv2\opencv.hpp>
#include <algorithm>
#include <opencv2/calib3d.hpp>
#include <opencv2/core.hpp>
#include <opencv2/imgcodecs.hpp>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/xfeatures2d/nonfree.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/core/utility.hpp>

using namespace cv;
using namespace cv::xfeatures2d;


size_t findMinIndex(std::vector<double>& data) {
    size_t min = 0;
    for (size_t i = 1; i != data.size(); ++i) {
        if (data[min] > data[i]) {
            min = i;
        }
    }
    return min;
}

double distance_between_words(std::vector <double> base, std::vector<double> input, int dimention) {
    double distance = 0;
    for (int i = 0; i < dimention; ++i) {
        distance += (double)(base[i] - input[i])*(double)(base[i] - input[i]);
    }
    return distance;
}

int main() {
    Mat list;
    std::vector<int> data_points(50);
    Ptr<Feature2D> f2d_sift = SIFT::create(0, 3, 0.18, 5, 1.6); //  trying cycles
    for (size_t i = 0; i != 50; ++i) {
        std::string filename = "kinopoisk/" + std::to_string(i) + ".jpg";
        Mat image = imread(filename);
        if (image.empty()) {
            continue;
        }
        Mat sized_image;
        resize(image, sized_image, Size(600, 800), 0, 0, INTER_LINEAR);
        std::vector<KeyPoint> points_one;
        Mat descriptor;
        f2d_sift->detectAndCompute(sized_image, Mat(), points_one, descriptor);
       // Mat sift_keys;
        data_points[i] = points_one.size();
        list.push_back(descriptor);
    }
    int K = list.rows / 80; // кол-во кластеров
    Mat centers;   // матрица, с K строками. В каждой строке 128 координат(потому что сифт).
    std::vector<int> labels(list.rows); //вектор на кластерах все особые точки во всех картинках.
    kmeans(list, K, labels, cvTermCriteria(CV_TERMCRIT_NUMBER + CV_TERMCRIT_EPS, 10, 1.0), 1, KMEANS_PP_CENTERS, centers);
    /*
        FileStorage fs_centers("data/fs_centers.yaml",FileStorage::WRITE);
        fs_centers << "cluster_centers" << centers;
        fs_centers.release();
        */
    int count = 0;
    std::vector<std::vector<int>> words(data_points.size(), std::vector<int>(K, 0));
    for (int i = 0; i < data_points.size(); ++i) {
        for (int j = 0; j < data_points[i]; ++j) {
            words[i][labels[count]]++;
            count++;
        }
    }
    /*
    for (int i = 0; i < data_points.size(); ++i) {
    for (int j = 0; j < K; ++j) {
    std::cout<< words[i][j]<< " ";                            WORDS
    }
    std::cout << std::endl << std::endl << std::endl << std::endl;
    }
    */
    std::vector<std::vector<double>> TF(data_points.size(), std::vector<double>(K, 0));

    for (int i = 0; i < TF.size(); ++i) {                     // TF[i]- тф представление итой картинки.
        for (int j = 0; j < TF[i].size(); ++j) {
            TF[i][j] = (double)words[i][j] / words[i].size(); 
        }
    }
    /*
    for (int i = 0; i < data_points.size(); ++i) {
    for (int j = 0; j < K; ++j) {
    std::cout << TF[i][j] << " ";                              TF
    }
    std::cout << std::endl << std::endl << std::endl << std::endl;
    }
    */
    std::vector<int> cluster_shmaster(K, 0);
    for (int i = 0; i < words.size(); ++i) {
        for (int j = 0; j < words[i].size(); ++j) {
            if (words[i][j] != 0)
                cluster_shmaster[j]++;
        }
    }
   

    std::vector<std::vector<double>> IDF(data_points.size(), std::vector<double>(K, 0));
    for (int i = 0; i < IDF.size(); ++i) {
        for (int j = 0; j < IDF[i].size(); ++j) {                     // IDF[i]- idf представление итой картинки.
            IDF[i][j] = (double)log((double)data_points.size() / cluster_shmaster[j]);
            //    std::cout << IDF[i][j] << " ";
        }
        //  std::cout << std::endl;
    }
    std::vector<std::vector<double>> TF_IDF(data_points.size(), std::vector<double>(K));
    for (int i = 0; i < IDF.size(); ++i) {
        for (int j = 0; j < IDF[i].size(); ++j) {
            TF_IDF[i][j] = (double)TF[i][j] * IDF[i][j];
        }
    }
    std::vector<std::vector<int>> base(K);
    for (int i = 0; i < K; ++i) {
        for (int j = 0; j < data_points.size(); j++) {
            if (TF[j][i] != 0)
                base[i].push_back(j);
        }
    }

    Mat test=imread("kinopoisk/1.jpg");
    if (test.empty())
        std::cout << "error";
    resize(test, test, Size(600, 800), 0, 0, INTER_LINEAR);
    std::vector<KeyPoint> test_points_one;
    Mat test_descriptor;
    f2d_sift->detectAndCompute(test, Mat(), test_points_one, test_descriptor);
    std::vector<int> test_labels(test_descriptor.rows);
    std::vector<std::vector<double>> distances(test_descriptor.rows, std::vector<double>(centers.rows, 0));

    double distance = 0;
    for (size_t j = 0; j != test_descriptor.rows; ++j) {
        float *currentDot = test_descriptor.ptr<float>(j);
        for (size_t i = 0; i != centers.rows; ++i) {
            float *currentRow = centers.ptr<float>(i);
            distance = normL2Sqr(currentDot, currentRow, test_descriptor.cols);
            distances[j][i] = distance;   // расстояние между j-ой точкой и i-тым кластером.
        }
    }

    for (size_t i = 0; i != test_labels.size(); ++i) {
        test_labels[i] = findMinIndex(distances[i]);
    }
    std::vector<double> test_visualWord(K, 0);
    for(int i = 0; i < test_labels.size(); ++i) {
            ++test_visualWord[labels[i]];
    }
    for (int i = 0; i < words.size(); ++i) {

    }
    std::cout << K << std::endl;
   // std::cout << cluster_shmaster[112] << " " << cluster_shmaster[113] << " " << cluster_shmaster[114] << " " << cluster_shmaster[111];
    std::vector<double> test_TF_IDF(data_points.size(), 0);
    for (int i = 0; i < K; ++i) {
        std::cout << i << std::endl;
        if (test_visualWord[i]!=0)
            test_TF_IDF[i] = ((double)(test_visualWord[i]) / test_labels.size())*(log((data_points.size()+1)/(cluster_shmaster[i]+1)));
        else
            test_TF_IDF[i] = ((double)(test_visualWord[i]) / test_labels.size())*(log((data_points.size() + 1) / (cluster_shmaster[i])));
    }

    int index = 0;
    double line = distance_between_words(TF_IDF[0], test_TF_IDF, K);
    for (int i = 1; i < data_points.size(); ++i) {
        if (line > distance_between_words(TF_IDF[i], test_TF_IDF, K)) {
            index = i;
            line = distance_between_words(TF_IDF[i], test_TF_IDF, K);
        }
    }
    std::cout << index << std::endl;
    
}

