﻿#include <opencv2/opencv.hpp>
#include <opencv2/nonfree/nonfree.hpp>
using namespace cv;
using std::cout;

void DetectKeypointsOnImage(const Mat& img,
    vector<KeyPoint>& keypoints)
{
    // загружаем изображение из файла 
    // инициализируем модуль nonfree для использования 
    // детектора SIFT 
    initModule_nonfree();
    // создаем SIFT детектор 
    Ptr<FeatureDetector> featureDetector =
        FeatureDetector::create("SIFT");
    // детектируем ключевые точки 
    // на загруженном изображении 
    featureDetector->detect(img, keypoints);
}


int main()
{

    Mat image, output;
    Mat reversed;
    image = imread("girl.jpg", CV_LOAD_IMAGE_UNCHANGED);

    if (!image.data)
    {
        cout << "Could not open or find the image" << std::endl;
        return 0;
    }

    namedWindow("Display window", CV_WINDOW_AUTOSIZE);
    imshow("Display window", image);

    bitwise_not(image, reversed);
    imwrite("reversedgirl.jpg", reversed);
    Point2f src_center(image.cols / 2.0, image.rows / 2.0);
    Mat rotation = getRotationMatrix2D(src_center, 30, 2.0 / 3);
    Mat rotated;
    warpAffine(image, rotated, rotation, image.size());
    Mat gray;
    Mat kp1, kp2;
    cvtColor(image, gray, CV_BGR2GRAY);

    int threshold1 = 50;
    std::vector<KeyPoint> keypoints1, keypoints2;
    FAST(gray, keypoints1, threshold1, true);
    drawKeypoints(image, keypoints1, kp1);

    imshow("rotatedgirl", rotated);
    imwrite("rotatedgirl.jpg", rotated);
    namedWindow("inversedgirl", WINDOW_AUTOSIZE);
    imshow("inversedgirl", reversed);
    

    namedWindow("kp1", WINDOW_AUTOSIZE);
    imshow("kp1", kp1);
    imwrite("kp1.jpg", kp1);
    /*
    DetectKeypointsOnImage(image, keypoints2);
    drawKeypoints(image, keypoints2, kp2);
    namedWindow("kp2", WINDOW_AUTOSIZE);
    imshow("kp2", kp2);
    */
    waitKey(0);
    return 0;
}
